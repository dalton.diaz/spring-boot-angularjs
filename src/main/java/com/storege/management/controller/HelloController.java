package com.storege.management.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@RequestMapping("/hello")
	public String getHello() {
	    return "home";
	}
	
	@RequestMapping("/app")
	public String app() {
	    return "hello";	
	}
	
	@RequestMapping("/marca")
	public String marca() {
	    return "angular-test/marca";	
	}
	
	@RequestMapping("/produto")
	public String produto() {
	    return "angular-test/produto";	
	}
	
	@RequestMapping("/lote")
	public String lote() {
	    return "angular-test/lote";	
	}
}
