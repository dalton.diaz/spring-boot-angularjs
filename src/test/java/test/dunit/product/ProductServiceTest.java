package test.dunit.product;

import static junit.framework.Assert.assertEquals;
import com.storege.management.product.Product;
import com.storege.management.product.ProductService;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.storege.management.AppSpringSecurityApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppSpringSecurityApplication.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class ProductServiceTest {
	
	@Inject
	private ProductService productService;
		
	@Test
	//TODO Verificar como ler a partir do test/resources.. se colocar no mesmo package da classe funciona
	@DatabaseSetup("product.xml")
	public void testFindAll()throws Exception{
		List<Product> list = productService.getAllProdcuts();
		assertEquals(1, list.size());
	}
	

}
