package com.storege.management.brand;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

@Component
public class BrandServiceImpl implements BrandService{

	
	@Inject
	private BrandRepository brandRepository;
	
	@Override
	public Brand findBrand(Long id) {
		return brandRepository.findOne(id);
	}

	@Override
	public List<Brand> findAllBrands() {
		return brandRepository.findAll();
	}

	@Override
	public void create(Brand b) {
		brandRepository.save(b);
	}

}
