package com.storege.management.brand;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BrandController {
	
	@Inject
	private BrandService brandService;
	
	@RequestMapping("/marcas")
	public ModelAndView getBrands(){
		return new ModelAndView("/brand/brands","brands",brandService.findAllBrands());
	}
	
	@RequestMapping("/marca/{id}")
	public ModelAndView getPage(@PathVariable Long id){
		return new ModelAndView("/brand/brand","form",brandService.findBrand(id));
	}
	
	@RequestMapping(value="/marca/novo",method=RequestMethod.GET)
	public ModelAndView getCreatePage(){
		return new ModelAndView("/brand/brand","form",new Brand());
	}
	
	@RequestMapping(value="/marca/novo", method=RequestMethod.POST)
	public String handleCreateForm(@Valid @ModelAttribute("form") Brand brand,BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return "/brand/brand";
		}
		try{
			brandService.create(brand);
		}catch(DataIntegrityViolationException e){
			bindingResult.reject("brand.exists","Marca já existe");
			return "/brand/brands";
		}
		return "redirect:/marcas";
	}
	
}
