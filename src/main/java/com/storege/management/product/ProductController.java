package com.storege.management.product;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.storege.management.brand.BrandService;

@Controller
public class ProductController {
	
	@Inject
	private ProductService productService;
	
	@Inject
	private BrandService brandService;
	
	@RequestMapping("/produtos")
	public ModelAndView getProductsPage(){
		return new ModelAndView("/product/products","products",productService.getAllProdcuts());
	}
	
	@RequestMapping("/produto/{id}")
	public ModelAndView getUserPage(@PathVariable Long id){
		ModelAndView model = new ModelAndView("/product/product","form",productService.getProductById(id));
		model.addObject("brands", brandService.findAllBrands());
		return model;
	}
	
	@RequestMapping(value="/produto/novo",method=RequestMethod.GET)
	public ModelAndView getUserCreatePage(){
		ModelAndView model = new ModelAndView("/product/product","form",new Product());
		model.addObject("brands", brandService.findAllBrands());
		return model;
	}
	
	@RequestMapping(value="/produto/novo", method=RequestMethod.POST)
	public String handleUserCreateForm(@Valid @ModelAttribute("form") Product product,BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return "/product/product";
		}
		try{
			productService.create(product);
		}catch(DataIntegrityViolationException e){
			bindingResult.reject("product.exists","Produto já existe");
			return "/product/product";
		}
		return "redirect:/produtos";
	}
	
	
}
