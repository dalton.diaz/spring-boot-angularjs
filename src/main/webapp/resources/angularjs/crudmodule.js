var crudApp = angular.module('crudApp', ['ngRoute'])
	.config(['$routeProvider',	function($routeProvider){
			$routeProvider.
				when('/produto',{
					templateUrl : '/produto',
					controller : 'productController'
				}).
				when('/lote',{
					templateUrl : '/lote',
					controller : 'batchController'
				}).
				when('/marca',{
					templateUrl : '/marca',
					controller : 'brandController'
				});
}]);

crudApp.controller('productController', function($scope) {
    $scope.p = 'Nescau';
});

crudApp.controller('batchController', function($scope) {
    $scope.batch = 'Caixa com 12';
});

crudApp.controller('brandController', function($scope) {
    $scope.name = 'Nestle';
});