<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="/resources/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <div class="container">
    <h1><spring:message code="batch.list" /></h1>
		<table class="table table-hover" >
		 	<thead>
	    		<tr>
		    		<th><spring:message code="batch.amount" /></th>
		    		<th><spring:message code="batch.validity" /></th>
		    		<th><spring:message code="batch.barCode" /></th>
		    		<th><spring:message code="batch.product" /></th>
	    		</tr>
		    </thead>
	    	<tbody>
	    	<c:forEach items="${batchs}" var="batch">
	    		<tr>
	    			<td>${batch.amount}</td>
	    			<td>${batch.validity}</td>
	    			<td>${batch.barCode}</td>
	    			<td>${batch.product.name}</td>
	    		</tr>
	   		</c:forEach>
	    	</tbody>
	    </table>

	    <a href="<spring:url value="/lote/novo" />"><spring:message code="batch.create" /></a>
    </div>
</body>
</html>