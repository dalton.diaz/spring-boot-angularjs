<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html >
    <head>
        <title>Hello World!</title>
        <link href="/resources/css/bootstrap.css" rel="stylesheet">
    	<script src="https://code.angularjs.org/1.3.15/angular.js" type="text/javascript"></script>
    	<script src="https://code.angularjs.org/1.3.15/angular-route.js" type="text/javascript" ></script>
        <script src="/resources/angularjs/crudmodule.js" type="text/javascript" ></script>
    </head>
    <body ng-app="crudApp">
       <div class="container">
        <div class="row">
        <div class="col-md-3">
            <ul class="nav">
                <li><a href="#marca"> Marcas </a></li>
                <li><a href="#produto"> Produtos </a></li>
                <li><a href="#lote"> Lotes </a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <div ng-view></div>
        </div>
        </div>
    </div>	
    
    </body>
    
</html>