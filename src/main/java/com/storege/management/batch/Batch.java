package com.storege.management.batch;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.storege.management.product.Product;

@Entity
@Table(name= "batch")
public class Batch implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY )
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "id_product", nullable = true)
	private Product product;
	
	@Column(name="amount",nullable=false)
	private Integer amount;
	
	@Enumerated
	@Column(name= "unit")
	private Unit unit;

	@Column(name= "validity")
	private Date validity;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(updatable = false,name="create_date")
	private Date createDate = new Date();
	
	@Column(name = "bar_code")
	private String barCode;
	
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Unit getUnit() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	public Date getValidity() {
		return validity;
	}
	public String getValidityDate(){
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(validity) ;
	}
	public void setValidity(Date validity) {
		this.validity = validity;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}
