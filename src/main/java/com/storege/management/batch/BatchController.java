package com.storege.management.batch;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.storege.management.product.ProductService;

@Controller
@RequestMapping("lote")
public class BatchController {

	@Inject
	private BatchService batchService;
	
	@Inject
	private ProductService productService;
	
	@RequestMapping("/lotes")
	public ModelAndView getBatchs(){
		return new ModelAndView("/batch/batchs","batchs",batchService.findAllBatch());
	}
	
	@RequestMapping("/{id}")
	public ModelAndView gethPage(@PathVariable Long id){
		ModelAndView model = new ModelAndView("/batch/batch","form",batchService.findBatch(id));
		model.addObject("products", productService.getAllProdcuts());
		return model;
	}
	
	@RequestMapping(value="/novo",method=RequestMethod.GET)
	public ModelAndView getCreatePage(){
		ModelAndView model = new ModelAndView("/batch/batch","form",new Batch());
		model.addObject("products", productService.getAllProdcuts());
		return model;
	}
	
	@RequestMapping(value="/novo", method=RequestMethod.POST)
	public String handleCreateForm(@Valid @ModelAttribute("form") Batch batch,BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return "/batch/batch";
		}
		try{
			batchService.create(batch);
		}catch(DataIntegrityViolationException e){
			bindingResult.reject("batchs.exists","Lote já existe");
			return "/batch/batchs";
		}
		return "redirect:/lote/lotes";
	}
	
	@RequestMapping(value = "/lotesj",method = RequestMethod.GET)
	public @ResponseBody String getListLotes(){
		try {
			return new ObjectMapper().writeValueAsString(batchService.findAllBatch());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
}
