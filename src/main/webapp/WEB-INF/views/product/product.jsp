

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<meta charset="utf-8">
<head>
	<link href="/resources/css/bootstrap.css" rel="stylesheet">
	<link href="/resources/css/datepicker.css" rel="stylesheet">
	<script src="/resources/js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="/resources/js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="/resources/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker();
		
		$('.autocomplete').autocomplete();
		
		$.ajax({
			url: "<spring:url value="/marcas" />",
			method: GET
		}).done(function(data){
			alert( "Data Saved: " + data );
		});
	})
	</script>
</head>
<body>
	<div class="container">
	    <h1><spring:message code="product.create" /></h1>
	    <a href="<spring:url value="/produtos" />"><spring:message code="product.list" /></a>
	    <form:form method="POST" action="/produto/novo" modelAttribute="form">
	        <form:errors path="" element="div" />
	        <form:hidden path="id"/>
	        <div>
	            <form:label path="name"><spring:message code="product.name" /></form:label>
	            <form:input path="name" />
	            <form:errors path="name" />
	        </div>
	        <div>
	            <form:label path="brand"><spring:message code="product.brand" /></form:label>
	            <form:select path="brand" id="select-product"  items="${ brands}" itemLabel="name" itemValue="id" /> 
	            <form:errors path="brand" />
	        </div>
	        
	        <div>
	            <input class="btn btn-success" type="submit"  value="Salvar"/>
	        </div>
	    </form:form>
    </div>	
</body>
</html>