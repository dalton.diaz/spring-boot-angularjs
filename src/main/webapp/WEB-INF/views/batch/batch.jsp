

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<meta charset="utf-8">
<head>
	<link href="/resources/css/bootstrap.css" rel="stylesheet">
	<link href="/resources/css/datepicker.css" rel="stylesheet">
	<script src="/resources/js/jquery-2.1.4.min.js" type="text/javascript"></script>
	<script src="/resources/js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="/resources/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker();
	})
	</script>
</head>
<body>
	<div class="container">
	    <h1><spring:message code="batch.create" /></h1>
	    <a href="<spring:url value="/lote/lotes" />"><spring:message code="batch.list" /></a>
	    <form:form method="POST" action="/lote/novo" modelAttribute="form">
	        <form:errors path="" element="div" />
	        <form:hidden path="id"/>
	        <div>
	            <form:label path="amount"><spring:message code="batch.amount" /></form:label>
	            <form:input path="amount" />
	            <form:errors path="amount" />
	        </div>
	        <div>
	            <form:label path="validity"><spring:message code="batch.validity" /></form:label>
	            <form:input path="validity" class="datepicker" data-date-format="mm/dd/yyyy" />
	            <form:errors path="validity" />
	        </div>
	        <div>
	            <form:label path="barCode"><spring:message code="batch.barCode" /></form:label>
	            <form:input path="barCode" />
	            <form:errors path="barCode" />
	        </div>
	        <div>
	            <form:label path="product"><spring:message code="batch.product" /></form:label>
	            <form:select path="product" id="select-product"  items="${ products}" itemLabel="name" itemValue="id" /> 
	            <form:errors path="product" />
	        </div>
	        <div>
	            <input class="btn btn-success" type="submit"  value="Salvar"/>
	        </div>
	    </form:form>
    </div>	
</body>
</html>