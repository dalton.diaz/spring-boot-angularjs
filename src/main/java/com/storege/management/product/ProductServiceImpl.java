package com.storege.management.product;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

@Component
public class ProductServiceImpl implements ProductService {
	
	@Inject
	private ProductRepository productRepository;

	@Override
	public Product getProductByName(String name) {
		return productRepository.findProductByName(name);
	}


	@Override
	public Product create(Product p) {
		return productRepository.save(p);
	}

	@Override
	public List<Product> getAllProdcuts() {
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(Long id) {
		return productRepository.findOne(id);
	}
	
	

}
