package com.storege.management.webservice;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.storege.management.batch.BatchService;
import com.storege.management.brand.Brand;
import com.storege.management.brand.BrandService;
import com.storege.management.product.Product;
import com.storege.management.product.ProductService;

@RestController
@RequestMapping("ws")
public class WebServiceController {
	
	@Inject
	private ProductService productService;
	
	@Inject
	private BrandService brandService;
	
	@Inject
	private BatchService batchService;
	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public List<Product> getListProducts(){
		return productService.getAllProdcuts();
	}
	@RequestMapping(value = "/brands", method = RequestMethod.GET)
	public List<Brand> getListBrands(){
		return brandService.findAllBrands();
	}
	
	public void getListBatchs(){
		
	}

}
