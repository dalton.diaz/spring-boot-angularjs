package com.storege.management.brand;

import java.util.List;

public interface BrandService {
	
	Brand findBrand(Long id);
	List<Brand> findAllBrands();
	void create(Brand b);
}
