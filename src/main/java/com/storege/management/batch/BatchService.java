package com.storege.management.batch;

import java.util.List;

public interface BatchService {
	
	List<Batch> findAllBatch();
	Batch findBatch(Long id);
	void create(Batch b);
	
}
