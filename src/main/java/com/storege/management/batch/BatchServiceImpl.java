package com.storege.management.batch;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

@Component
public class BatchServiceImpl implements BatchService {

	@Inject
	private BatchRepository batchRepository;
	
	@Override
	public List<Batch> findAllBatch() {
		return batchRepository.findAll();
	}

	@Override
	public Batch findBatch(Long id) {
		return batchRepository.findOne(id);
	}

	@Override
	public void create(Batch b) {
		batchRepository.save(b);
	}

}
