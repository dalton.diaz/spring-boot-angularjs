package com.storege.management.product;

import java.util.List;

public interface ProductService {
	
	Product getProductByName(String name);
	Product create(Product p);
	List<Product> getAllProdcuts();
	Product getProductById(Long id);
	
}
