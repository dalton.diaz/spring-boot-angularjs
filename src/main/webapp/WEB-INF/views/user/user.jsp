<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<head><link href="/resources/css/bootstrap.css" rel="stylesheet"></head>
<body>
	<div class="container">
	    <h1><spring:message code="user.create" /></h1>
	    <a href="<spring:url value="/users" />"><spring:message code="user.list" /></a>
	    <form:form method="POST" action="/user/create" modelAttribute="form" cssClass="form-horizontal">
	        <form:hidden path="id"/>
	        <div class="form-group">
	            <label class="col-sm-2 control-label" ><spring:message code="user.username" /></label>
	            <div class="col-md-4">
	            	<form:input path="name" cssClass="form-control"/>
	            </div>
	            <form:errors path="name" />
	        </div>
	        <div class="form-group">
	            <label class="col-sm-2 control-label" ><spring:message code="user.email" /></label>
	            <div class="col-md-4">	            
	           		<form:input path="email" cssClass="form-control" />
	            </div>
	            <form:errors path="email" />
	        </div>
	        <div class="form-group">
	            <label class="col-sm-2 control-label" ><spring:message code="user.password" /></label>
	            <div class="col-md-4">
	            	<form:password path="password" cssClass="form-control" />
	            </div>
	            <form:errors path="password" />
	        </div>
	        <div class="form-group">
	            <label class="col-sm-2 control-label" ><spring:message code="user.passwordRepeated" /></label>
	            <div class="col-md-4">
	            	<form:password path="passwordRepeated" cssClass="form-control"  />
	            </div>
	            <form:errors path="passwordRepeated" />
	        </div>
	        <div class="form-group">
	        	<div class="col-sm-offset-2 col-sm-10">
		            <input class="btn btn-success" type="submit" />
	        		<form:errors path="" element="div" />
	        	</div>
	        </div>
	    </form:form>
    </div>
</body>
</html>