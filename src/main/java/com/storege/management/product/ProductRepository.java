package com.storege.management.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{
	
	Product findProductByName(String name);
	
	@Query("SELECT p FROM Product p")
	List<Product> findAllProductsByOrderValidateDate();
}
